# SISC Authors per Paper #

One afternoon I was curious about how many co-authors there are on a "typical" paper in the area of computational science and engineering (CSE). So, I scraped some data from the [SIAM Journal on Scientific Computing (SISC)](http://www.siam.org/journals/sisc.php).

The main "result" is in the figure, `doubled.pdf`, in the sources of this repository.

### Reproducing the figure ###

Use `download.bash` to download the raw HTML files containing the tables-of-contents for issues of SISC. (Alternatively, just use the pre-downloaded files in the `raw` subdirectory of this repo.) Use `analyze.py` to extract the total number of papers and authors per year. Use `analyze.R` to make the figure.
