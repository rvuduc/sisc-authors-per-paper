#!/usr/bin/env python

import sys
import os
import re

outfilename = None
if len (sys.argv) == 2:
    outfilename = sys.argv[1]

fileMatcher = re.compile ('^.*\.html$')
yearMatcher = re.compile ('^.*<input type="hidden" name="title" value="SIAM Journal on Scientific .*Computing \(([0-9]+)\)" />$')

ROOT = 'raw'
Files = os.listdir (ROOT)

# Determine, for each file, its year
YearOfFile = {}
for filename in Files:
    if not fileMatcher.match (filename): continue
    print ("=== Processing: %s ===" % filename)

    fp = open ("%s/%s" % (ROOT, filename), 'rt')
    print ("Opening...")
    for line in fp.readlines ():
        hasYear = yearMatcher.match (line)
        if hasYear:
            year = int (hasYear.groups ()[0])
            YearOfFile[filename] = year
    fp.close ()

countMatcher = re.compile ('^.*--totalCount([0-9]+)--.*$')
authorMatcher = re.compile ('class="entryAuthor"')
curYear = -1
curPaperCount = 0
curAuthorsCount = 0
PaperCountOfYear = {}
AuthorsCountOfYear = {}
for filename in Files:
    if not fileMatcher.match (filename): continue
    thisYear = YearOfFile[filename]
    print ("\t'%s': %d [%d:%d]" % (filename, thisYear, curYear, curPaperCount))
    if thisYear != curYear:
        if curYear > 0:
            PaperCountOfYear[curYear] = curPaperCount
            AuthorsCountOfYear[curYear] = curAuthorsCount
        curYear = thisYear
        curPaperCount = 0
        curAuthorsCount = 0

    fp = open ("%s/%s" % (ROOT, filename), 'rt')
    for line in fp.readlines ():
        countFound = countMatcher.match (line)
        if countFound:
            curPaperCount = curPaperCount + int (countFound.groups ()[0])
            continue

        authorsFound = authorMatcher.findall (line)
        if authorsFound:
            curAuthorsCount = curAuthorsCount + len (authorsFound)
    fp.close ()

if curYear > 0:
    PaperCountOfYear[curYear] = curPaperCount
    AuthorsCountOfYear[curYear] = curAuthorsCount

print ("=== Counts ===")
Years = list(PaperCountOfYear.keys()) ; Years.sort ()
print ("Years: %s" % Years)
for year in Years:
    paperCount = PaperCountOfYear[year]
    authorsCount = AuthorsCountOfYear[year]
    authorsPerPaper = float (authorsCount) / paperCount
    print ("\t%d: %d papers and %d authors [%f]" \
           % (year, paperCount, authorsCount, authorsPerPaper))

if outfilename:
    print ("--> Final output will go to: '%s'" % outfilename)
    fp = open (outfilename, 'wt')
    fp.write ("Year,Papers,Authors\n")
    for year in Years:
        paperCount = PaperCountOfYear[year]
        authorsCount = AuthorsCountOfYear[year]
        fp.write ("%d,%d,%d\n" % (year, paperCount, authorsCount))
    fp.close ()

# eof
