#!/usr/bin/env bash

rm -f LOG
touch LOG
#for VOLUME in 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 ; do
#for VOLUME in 37 ; do
for VOLUME in 38 39 40 41 42 ; do
    if test ${VOLUME} -le 13 ; then
	PREFIX=sijcd4
    else
	PREFIX=sjoce3
    fi
    for ISSUE in 1 2 3 4 5 6 ; do
	OUTFILE=raw/${VOLUME}.${ISSUE}.html
	echo "=== SISC v${VOLUME}:${ISSUE} [${OUTFILE}] ==="

	if test -f "${OUTFILE}" ; then
	    echo "   (already exists; skipping...)"
	elif ! wget 'http://epubs.siam.org/toc/'${PREFIX}'/'${VOLUME}'/'${ISSUE} -a LOG -O ${OUTFILE} ; then
	    echo "   *** (not found) ***"
	    rm -f ${OUTFILE}
	else
	    echo "   (downloaded; pausing briefly ...)"
	    sleep 15
	fi
    done # ISSUE
done # VOLUME

# eof
